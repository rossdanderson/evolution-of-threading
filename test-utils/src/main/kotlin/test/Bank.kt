package test

import adapter.BankTransfer
import adapter.IBank
import com.github.javafaker.Company
import com.github.javafaker.Faker
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import java.util.*
import kotlin.random.Random

internal class Bank(
    private val debug: Boolean
) : IBank {

    private val faker = Faker()
    private val semaphore = Semaphore(64)

    override fun getTransfers(userId: UUID): List<BankTransfer> = runBlocking {
        semaphore.withPermit {
            if (debug) println("Getting transfers for $userId")

            delay(1250)

            (0..Random.nextInt(100, 300)).map {
                BankTransfer(
                    transferId = UUID.randomUUID(),
                    amount = Random.nextInt(10000, 100000).toBigDecimal(),
                    transferType = if (Random.nextInt(10) >= 9) BankTransfer.TransferType.INFLOW else BankTransfer.TransferType.OUTFLOW,
                    companyId = UUID.randomUUID()
                )
            }
        }
    }

    override fun getCompany(id: UUID): Company = runBlocking {
        semaphore.withPermit {
            if (debug) println("Getting company for $id")

            delay(15)
            faker.company()
        }
    }
}
