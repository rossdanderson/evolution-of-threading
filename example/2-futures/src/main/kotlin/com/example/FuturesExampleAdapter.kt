package com.example

import adapter.*
import com.github.javafaker.Company
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors

class FuturesExampleAdapter : IAdapter {

    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        val futuresBank = FuturesBank(bankApi)
        while (true) {
            val request = clientApi.requestChannel.poll() ?: break

            futuresBank
                .getTransfers(request.userId)
                // thenApplyAsync works on a background pool of threads by default - though an explicit executor can
                // be provided
                .thenApplyAsync { transfers ->
                    val asyncClientTransfers = transfers
                        .map { bankTransfer ->
                            // To speed things up even further, we can launch an async task to request each company
                            // in parallel
                            futuresBank
                                .getCompany(bankTransfer.companyId)
                                .thenApplyAsync { company ->
                                    ClientTransfer(
                                        bankTransfer,
                                        company
                                    )
                                }
                        }

                    CompletableFuture
                        .allOf(*asyncClientTransfers.toTypedArray()) // Wait for all client transfers to be created
                        .thenApplyAsync {
                            val clientTransfers = asyncClientTransfers.map(CompletableFuture<ClientTransfer>::join)
                            clientApi.responseChannel.send(Response(request.requestId, clientTransfers))
                        }
                }
        }
    }
}

class FuturesBank(
    private val bankApi: IBank,
) {
    // We create a thread pool for all long running bank operations. We are going to set this to 64 threads.
    private val bankExecutor = Executors.newFixedThreadPool(64)

    // We offload all bank operations onto the bank executor thread pool
    // so that we do not block threads the main application's shared thread pool
    fun getTransfers(userId: UUID): CompletableFuture<List<BankTransfer>> =
        CompletableFuture.supplyAsync({ bankApi.getTransfers(userId) }, bankExecutor)

    fun getCompany(companyId: UUID): CompletableFuture<Company> =
        CompletableFuture.supplyAsync({ bankApi.getCompany(companyId) }, bankExecutor)
}
