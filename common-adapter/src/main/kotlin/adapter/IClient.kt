package adapter

interface IClient {
    val requestChannel: IRequestChannel
    val responseChannel: IResponseChannel
}
