package com.example

import adapter.IAdapter
import org.junit.jupiter.api.Test
import test.TestUtil.makeRequests

class ReactiveAdapterTest {

    private val adapter: IAdapter = ReactiveAdapter()

    @Test
    fun `Ten requests`() {
        makeRequests(10, adapter)
    }

    @Test
    fun `One hundred requests`() {
        makeRequests(100, adapter)
    }

    @Test
    fun `One thousand requests`() {
        makeRequests(1_000, adapter)
    }

    @Test
    fun `Ten thousand requests`() {
        makeRequests(10_000, adapter)
    }
}
