package com.example

import adapter.IAdapter
import org.junit.jupiter.api.Test
import test.TestUtil.makeRequests

class BlockingExampleAdapterTest {

    private val adapter: IAdapter = BlockingExampleAdapter()

    @Test
    fun `Ten requests`() {
        makeRequests(10, adapter)
    }
}
