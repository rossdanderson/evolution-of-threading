package com.example

import adapter.*
import com.github.javafaker.Company
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.util.*

class ReactiveExampleAdapter : IAdapter {

    @Suppress("UNCHECKED_CAST")
    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        val reactiveBank = ReactiveBank(bankApi)

        Flux
            .create<Request> { emitter ->
                // This is quite unique compared to the other examples - we only poll when the downstream is capable
                // of handling more requests.
                // This means that we can signal to the upstream that we are under pressure, and that we can use
                // techniques such as conflation (where appropriate) to reduce the amount of work that the system has to
                // do overall, and avoid the system becoming overwhelmed. This is referred to as backpressure.
                emitter.onRequest { count ->
                    for (i in 0..count) {
                        val request = clientApi.requestChannel.poll()
                        if (request != null) emitter.next(request) else {
                            emitter.complete()
                            break
                        }
                    }
                }
            }
            .flatMap { request ->
                reactiveBank.getTransfers(request.userId)
                    .flatMap { transfers ->
                        // Another nice thing about Mono and Flux is that when you are combining them through an operator
                        // such as zip, if any of the streams being combined returns an error, it will automatically
                        // cancel all remaining tasks for all of the other Monos/Fluxes, since it would be pointless to
                        // continue.
                        Mono.zip(
                            transfers.map { bankTransfer ->
                                reactiveBank.getCompany(bankTransfer.companyId)
                                    .map { company -> ClientTransfer(bankTransfer, company) }
                            }
                        ) { clientTransfers ->
                            Response(
                                request.requestId,
                                clientTransfers.toList() as List<ClientTransfer>
                            )
                        }
                    }
            }
            .subscribe { response -> clientApi.responseChannel.send(response) }
    }
}

class ReactiveBank(
    private val bankApi: IBank,
) {
    private val scheduler = Schedulers.newParallel("bank", 64)

    // We offload all bank operations onto the bank executor thread pool
    // so that we do not block threads the main application's shared thread pool
    // Once we have a result, we publish it to subscribers to resume their work on the default shared parallel thread pool.
    fun getTransfers(userId: UUID): Mono<List<BankTransfer>> {
        return Mono.fromCallable { bankApi.getTransfers(userId) }
            .subscribeOn(scheduler)
            .publishOn(Schedulers.parallel())
    }

    fun getCompany(companyId: UUID): Mono<Company> =
        Mono.fromCallable { bankApi.getCompany(companyId) }
            .subscribeOn(scheduler)
            .publishOn(Schedulers.parallel())
}
