import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    `java-library`
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

dependencies {
    api(project(":common-adapter"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.1")
}
