package com.example

import adapter.IAdapter
import org.junit.jupiter.api.Test
import test.TestUtil.makeRequests

class BlockingAdapterTest {

    private val adapter: IAdapter = BlockingAdapter()

    @Test
    fun `Ten requests`() {
        makeRequests(10, adapter)
    }
}
