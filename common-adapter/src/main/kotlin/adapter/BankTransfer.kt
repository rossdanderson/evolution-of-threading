package adapter

import java.math.BigDecimal
import java.util.*

data class BankTransfer(
    val transferId: UUID,
    val amount: BigDecimal,
    val transferType: TransferType,
    val companyId: UUID,
) {
    enum class TransferType {
        INFLOW,
        OUTFLOW
    }
}
