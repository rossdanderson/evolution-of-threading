package adapter

interface IResponseChannel {
    fun send(response: Response)
}
