import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    implementation(platform("io.projectreactor:reactor-bom:2020.0.3"))
    implementation("io.projectreactor:reactor-core")
    implementation("io.projectreactor:reactor-tools")
    implementation(project(":common-adapter"))
    testImplementation(project(":test-utils"))
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
}
