package com.example

import adapter.*

class BlockingExampleAdapter : IAdapter {

    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        while (true) {
            val request = clientApi.requestChannel.poll() ?: break

            val clientTransfers = bankApi.getTransfers(request.userId)
                .map { bankTransfer ->
                    ClientTransfer(
                        bankTransfer,
                        bankApi.getCompany(bankTransfer.companyId)
                    )
                }

            clientApi.responseChannel.send(Response(request.requestId, clientTransfers))
        }
    }
}
