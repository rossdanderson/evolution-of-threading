package com.example

import adapter.IAdapter
import adapter.IBank
import adapter.IClient

class FuturesAdapter : IAdapter {

    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        // We need to:
        // * Poll the request channel provided by the Client API for each incoming request from the client application
        // * Retrieve the user's BankTransfers from the (slow) Bank API
        // * Fetch additional Company information for each BankTransfer
        // * Map this data into corresponding ClientTransfer objects
        // * Send the response to the client application on the response channel

        TODO("Not yet implemented")
    }
}
