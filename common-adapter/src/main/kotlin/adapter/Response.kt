package adapter

import java.util.*

data class Response(
    val requestId: UUID,
    val clientTransfers: List<ClientTransfer>,
)
