rootProject.name = "managing-concurrency"

include(
    ":exercise:0-blocking",
    ":exercise:1-thread-per-request",
    ":exercise:2-futures",
    ":exercise:3-reactive",
    ":exercise:4-coroutines",
    ":example:0-blocking",
    ":example:1-thread-per-request",
    ":example:2-futures",
    ":example:3-reactive",
    ":example:4-coroutines",
    ":common-adapter",
    ":test-utils"
)
