package adapter

interface IAdapter {

    fun initialise(
        bankApi: IBank,
        clientApi: IClient
    )
}
