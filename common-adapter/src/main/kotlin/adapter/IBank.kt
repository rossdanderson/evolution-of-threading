package adapter

import com.github.javafaker.Company
import java.util.*

interface IBank {

    fun getTransfers(userId: UUID): List<BankTransfer>

    fun getCompany(id: UUID): Company
}
