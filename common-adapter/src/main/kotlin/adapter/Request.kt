package adapter

import java.util.*

data class Request(
    val requestId: UUID,
    val userId: UUID
)
