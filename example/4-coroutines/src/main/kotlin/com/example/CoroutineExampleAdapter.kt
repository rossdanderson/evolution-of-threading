package com.example

import adapter.*
import com.github.javafaker.Company
import kotlinx.coroutines.*
import java.util.*

class CoroutineExampleAdapter : IAdapter {

    private val scope = CoroutineScope(Dispatchers.Default)

    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        val coroutineBank = CoroutineBank(bankApi)
        while (true) {
            val request = clientApi.requestChannel.poll() ?: break

            // We launch one coroutine per request so that we can handle multiple requests in parallel
            // This looks like the one thread per request model, but is far more lightweight
            scope.launch {

                val clientTransfers = coroutineBank.getTransfers(request.userId)
                    .map { bankTransfer ->
                        // To speed things up even further, we can launch a job for to request each company
                        // in parallel. Again this looks like launching and joining a thread.
                        async {
                            ClientTransfer(
                                bankTransfer,
                                coroutineBank.getCompany(bankTransfer.companyId)
                            )
                        }
                    }
                    .awaitAll() // Suspend here for all the async blocks to complete. This does not block a thread.

                clientApi.responseChannel.send(Response(request.requestId, clientTransfers))
            }
        }
    }
}

class CoroutineBank(
    private val bankApi: IBank
) {
    //For slow requests that block on IO - networking, disk access etc. - we move onto an IO dispatcher
    suspend fun getTransfers(userId: UUID): List<BankTransfer> = withContext(Dispatchers.IO) {
        bankApi.getTransfers(userId)
    }

    suspend fun getCompany(companyId: UUID): Company = withContext(Dispatchers.IO) {
        bankApi.getCompany(companyId)
    }
}
