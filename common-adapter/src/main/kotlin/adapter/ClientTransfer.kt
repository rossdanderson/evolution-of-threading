package adapter

import com.github.javafaker.Company
import java.math.BigDecimal
import java.util.*

data class ClientTransfer(
    val transferId: UUID,
    val companyName: String,
    val amount: BigDecimal,
    val transferType: TransferType
) {
    constructor(
        bankTransfer: BankTransfer,
        company: Company
    ) : this(
        bankTransfer.transferId,
        company.name(),
        bankTransfer.amount,
        when (bankTransfer.transferType) {
            BankTransfer.TransferType.INFLOW -> TransferType.INFLOW
            BankTransfer.TransferType.OUTFLOW -> TransferType.OUTFLOW
        }
    )

    enum class TransferType {
        INFLOW,
        OUTFLOW
    }
}
