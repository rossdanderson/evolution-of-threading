package adapter

interface IRequestChannel {
    /**
     * Blocks until a new request is available, returns null when the channel is closed
     */
    fun poll(): Request?
}
