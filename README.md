# Managing Concurrency on the JVM

The project is structured into two main directories - examples and exercises.

The exercises directory contains a skeleton application, and some tests, for you to implement each of the following concurrency models:
* Blocking
* Thread per request
* Futures
* Reactive
* Coroutines (non-reactive)

Implement each of these in order, and run the tests with a profiler attached. Pay attention to throughput, memory usage etc. What can you infer?

The examples directory contains a reference implementation of each. Try not to look at these until you have implemented your own.

How is the reactive implementation different to the others? Pay attention to the test output. How might this be a benefit?
