package test

import adapter.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.math.RoundingMode.HALF_DOWN
import java.math.RoundingMode.HALF_UP
import java.time.Duration
import java.time.Instant
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

object TestUtil {

    private val testDispatcher = Executors.newCachedThreadPool().asCoroutineDispatcher()

    data class TimestampedResponse(
        val response: Response
    ) {
        val responseTime: Instant = Instant.now()
    }

    fun makeRequests(
        num: Int,
        adapter: IAdapter,
        timeoutMillis: Long = 100000,
        debugRequests: Boolean = false,
        debugBank: Boolean = false
    ) {
        runBlocking {

            val scope = CoroutineScope(testDispatcher)

            withTimeout(timeoutMillis) {
                val responseFlow =
                    if (debugRequests) MutableSharedFlow<TimestampedResponse>(extraBufferCapacity = num) else null

                var nextRequestTime = Instant.now()
                var numSent = 0
                val _numReceived = AtomicInteger(0)

                scope.launch {
                    var i = 0
                    var lastNumSent = 0
                    var lastNumReceived = 0
                    while (true) {
                        delay(5000)
                        val seconds = (i++ + 1) * 5

                        val sentInPeriod = numSent - lastNumSent
                        val sentOps = sentInPeriod.toDouble() / 5
                        lastNumSent = numSent
                        val numReceived = _numReceived.get()

                        val receivedInPeriod = numReceived - lastNumReceived
                        val receivedOps = receivedInPeriod.toDouble() / 5
                        lastNumReceived = numReceived
                        println(
                            "${seconds}s\n" +
                                    "\t$sentInPeriod requests sent (${
                                        sentOps.toBigDecimal().setScale(2, HALF_UP).toPlainString()
                                    }ops/s) - $numSent total\n" +
                                    "\t$receivedInPeriod responses received (${
                                        receivedOps.toBigDecimal().setScale(2, HALF_UP).toPlainString()
                                    }ops/s) - $numReceived total\n"
                        )
                    }
                }

                val start = Instant.now()
                adapter.initialise(
                    Bank(debugBank),
                    object : IClient {
                        override val requestChannel: IRequestChannel = object : IRequestChannel {

                            override fun poll(): Request? {
                                return if (numSent < num) {
                                    val pollTime = Instant.now()
                                    Thread.sleep(
                                        Duration.between(pollTime, nextRequestTime).toMillis().coerceAtLeast(0)
                                    )

                                    val requestId = UUID.randomUUID()
                                    val requestTime = nextRequestTime
                                    val sendTime = Instant.now()

                                    responseFlow
                                        ?.filter { it.response.requestId == requestId }
                                        ?.map { it.responseTime }
                                        ?.onEach { responseTime ->
                                            val totalDuration = Duration.between(requestTime, responseTime)
                                            val processDuration = Duration.between(sendTime, responseTime)

                                            val queueDuration = totalDuration - processDuration

                                            println("Response for $requestId took $totalDuration - Queued for $queueDuration")
                                        }?.withIndex()
                                        ?.onEach { if (it.index >= 1) throw IllegalStateException("Received multiple responses for $requestId") }
                                        ?.launchIn(scope)

                                    if (debugRequests) {

                                        println("Sending request $numSent $requestId")
                                    }

                                    nextRequestTime = requestTime.plusMillis(5)
                                    numSent++

                                    Request(
                                        requestId = requestId,
                                        userId = UUID.randomUUID()
                                    )
                                } else null
                            }
                        }
                        override val responseChannel: IResponseChannel = object : IResponseChannel {
                            override fun send(response: Response) {
                                _numReceived.incrementAndGet()
                                responseFlow?.tryEmit(TimestampedResponse(response))
                            }
                        }
                    }
                )

                while (numSent < num || numSent != _numReceived.get()) yield()


                val duration = Duration.between(start, Instant.now()).toSeconds()
                val receivedOps = _numReceived.get().toDouble() / duration

                println(
                    "Total time taken: ${duration}s - ${
                        receivedOps.toBigDecimal().setScale(2, HALF_DOWN).toPlainString()
                    }ops/s"
                )
                scope.cancel()
            }
        }
    }
}
