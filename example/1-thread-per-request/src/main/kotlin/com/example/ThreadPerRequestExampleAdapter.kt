package com.example

import adapter.*

class ThreadPerRequestExampleAdapter : IAdapter {

    override fun initialise(
        bankApi: IBank,
        clientApi: IClient
    ) {
        while (true) {
            val request = clientApi.requestChannel.poll() ?: break

            Thread {
                val clientTransfers = bankApi.getTransfers(request.userId)
                    .map { bankTransfer ->
                        // I'm not going to launch a thread per request here, because that would be crazy.
                        ClientTransfer(
                            bankTransfer,
                            bankApi.getCompany(bankTransfer.companyId)
                        )
                    }

                clientApi.responseChannel.send(Response(request.requestId, clientTransfers))
            }.start()
        }
    }
}
